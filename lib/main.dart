import 'package:flutter/material.dart';
import 'screens/home_screen.dart';

void main() {
  runApp(MeterReadingApp());
}

class MeterReadingApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Meter Reading App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeScreen(),
    );
  }
}