import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'image_display_screen.dart';

class LibraryScreen extends StatefulWidget {
  @override
  _LibraryScreenState createState() => _LibraryScreenState();
}

class _LibraryScreenState extends State<LibraryScreen> {
  File? _image;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      getImage(); // 页面渲染完成后，直接调用 getImage
    });
  }

  Future getImage() async {
    final pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Image Picker Example'),
        backgroundColor: Color(0xff559f4c), // 使用给定的颜色代码
      ),
      body: ListView(
        children: [
          Center(
            child: _image == null
                ? null
                : Column(
              children: [
                Container(
                  color: Colors.black.withOpacity(0.5),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center, // 设置对齐方式为居中
                      children: [
                        Expanded(
                          child: Text(
                            'Please ensure meter photo is readable',
                            textAlign: TextAlign.center, // 设置文本居中对齐
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Image.file(_image!),
                AdditionalInfoForm(), // 这个是你的其他信息表单
              ],
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getImage,
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
        backgroundColor: Color(0xff559f4c), // 使用给定的颜色代码
      ),
    );
  }
}
