import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'dart:io';
import 'image_display_screen.dart';

class CameraScreen extends StatefulWidget {
  @override
  _CameraScreenState createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {
  late CameraController _controller;
  Future<void>? _initializeControllerFuture;
  String? imagePath;
  final TextEditingController emailController = TextEditingController();
  String? meterType;

  @override
  void initState() {
    super.initState();
    _initializeControllerFuture = _initializeController();
  }

  Future<void> _initializeController() async {
    final cameras = await availableCameras();
    final firstCamera = cameras.first;

    _controller = CameraController(
      firstCamera,
      ResolutionPreset.medium,
    );

    return await _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Camera Screen'),
        backgroundColor: Color(0xff559f4c), // 使用给定的颜色代码
      ),
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return ListView(
              children: [
                Center(
                  child: imagePath == null
                      ? CameraPreview(_controller)
                      : Column(
                        children: [
                          Container(
                            color: Colors.black.withOpacity(0.5),
                            width: double.infinity,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center, // 设置对齐方式为居中
                                children: [
                                  Expanded(
                                    child: Text(
                                      'Please ensure meter photo is readable',
                                      textAlign: TextAlign.center,  // 设置文本居中对齐
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Image.file(
                        File(imagePath!),
                        width: 300,  // 缩小宽度
                        height: 300, // 缩小高度
                        fit: BoxFit.contain,
                      ),
                        AdditionalInfoForm(),  // 这是你的其他信息表单
                    ],
                  ),
                ),
              ],
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          try {
            await _initializeControllerFuture;
            final image = await _controller.takePicture();
            setState(() {
              imagePath = image.path;
            });
          } catch (e) {
            print("Error taking picture: $e");
          }
        },
        child: Icon(Icons.camera),
        backgroundColor: Color(0xff559f4c), // 使用给定的颜色代码
      ),
    );
  }
}
