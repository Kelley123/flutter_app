import 'package:flutter/material.dart';
import 'camera_screen.dart';  // 假设这是您的相机屏幕
import 'upload_screen.dart';  // 假设这是您的上传屏幕

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Meter Reading App'),
        backgroundColor: Color(0xff559f4c), // 使用给定的颜色代码
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // 添加这个 Image 组件
            Transform.translate(
              offset: Offset(0, -50), // 上移50个逻辑像素
              child: Image.asset(
                'images/home.png', // 替换为你的第一个图像路径
                width: 100, // 你可以设置你希望的宽度
                height: 100, // 和高度
              ),
            ),
            Transform.translate(
              offset: Offset(0, -60), // 再次上移50个逻辑像素
              child: Image.asset(
                'images/data.png', // 替换为你的第二个图像路径
                width: 200, // 你可以设置你希望的宽度
                height: 100, // 和高度
              ),
            ),
            SizedBox(height: 0), // 也可以调整这个大小来改变图像与按钮之间的间距
            SizedBox(
              width: 200,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CameraScreen()),
                  );
                },
                child: Text('Take a Photo'),
                style: ElevatedButton.styleFrom(
                  primary: Color(0xFF64eac5), // 主要背景颜色
                  onPrimary: Colors.white, // 文字颜色
                ),
              ),
            ),
            SizedBox(height: 20),
            SizedBox(
              width: 200,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LibraryScreen()),
                  );
                },
                child: Text('Select from Library'),
                style: ElevatedButton.styleFrom(
                  primary: Color(0xFF6f42c1), // 主要背景颜色
                  onPrimary: Colors.white, // 文字颜色
                ),
              ),
            ),
          ],
        ),
      ),
      backgroundColor: Color(0xffddeff1), // 设置背景颜色
    );
  }
}
