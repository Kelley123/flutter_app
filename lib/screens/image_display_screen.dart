import 'dart:async';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Additional Info Form"),
          backgroundColor: Color(0xFF3cbac5),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: AdditionalInfoForm(),
        ),
      ),
    );
  }
}

class AdditionalInfoForm extends StatefulWidget {
  @override
  _AdditionalInfoFormState createState() => _AdditionalInfoFormState();
}

class _AdditionalInfoFormState extends State<AdditionalInfoForm> {
  final TextEditingController emailController = TextEditingController();
  String? meterType;
  bool showEmailTooltip = false;
  bool showMeterTypeTooltip = false;
  bool showLoading = false;
  bool showSuccess = false;

  _showSuccessDialog() {
    setState(() {
      showLoading = true;
    });

    // Simulate network delay
    Timer(Duration(seconds: 2), () {
      setState(() {
        showLoading = false;
        showSuccess = true;
      });
      // Reset after showing the success checkmark
      Timer(Duration(seconds: 2), () {
        showSuccess = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'Additional Information',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 24,
            color: Color(0xFF3cbac5),
          ),
        ),
        showEmailTooltip ? Padding(
          padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
          child: Text(
            'Email address is required. Use the address associated with your Energy Elephant account',
            style: TextStyle(color: Colors.red),
          ),
        ) : Container(),

        showMeterTypeTooltip ? Padding(
          padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
          child: Text(
            'Please select a meter type.',
            style: TextStyle(color: Colors.red),
          ),
        ) : Container(),
        TextFormField(
          controller: emailController,
          decoration: InputDecoration(labelText: 'Enter your email address'),
        ),
        SizedBox(height: 20),
        Row(
          children: [
            Expanded(
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    meterType = 'gas';
                  });
                },
                child: Text('Gas'),
                style: ElevatedButton.styleFrom(
                  primary: meterType == 'gas' ? Color(0xFF64eac5) : Colors.grey,
                ),
              ),
            ),
            Expanded(
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    meterType = 'electric';
                  });
                },
                child: Text('Electric'),
                style: ElevatedButton.styleFrom(
                  primary:
                  meterType == 'electric' ? Color(0xFF64eac5) : Colors.grey,
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Container(
          width: double.infinity,
          child: ElevatedButton(
            onPressed: () {
              setState(() {
                showEmailTooltip = emailController.text.isEmpty;
                showMeterTypeTooltip = meterType == null;

                if (!showEmailTooltip && !showMeterTypeTooltip) {
                  _showSuccessDialog();
                }
              });
            },
            child: showLoading
                ? CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            )
                : (showSuccess
                ? Icon(Icons.check, color: Colors.white)
                : Text('Submit')),
            style: ElevatedButton.styleFrom(
              primary: Color(0xFF3cbac5),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              elevation: 3,
            ),
          ),
        ),
      ],
    );
  }
}
